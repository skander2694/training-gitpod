#!/usr/bin/bash

set -euET -o pipefail

ligo compile contract examples/add_number/add_number.jsligo
ligo compile contract examples/charity/charity.jsligo
ligo run test examples/charity/charity_test.jsligo
ligo compile contract examples/counter/counter.jsligo
ligo compile contract examples/counter_two_numbers/counter_two_numbers.jsligo
ligo compile contract examples/verifications/account.jsligo
ligo compile contract examples/visitors/visitors.jsligo
ligo compile contract examples/visitors/visitors.jsligo
ligo compile contract exercises/auction_flaws/auction.jsligo
ligo run test exercises/auction_flaws/auction_test.jsligo
ligo compile contract exercises/timelock_flaw/timelock.jsligo
ligo compile contract exercises/timelock_flaw/timelock.jsligo